#define ANIMATE_LED 0

#include <SoftPWM.h>

#define NUMLEDS 6

SOFTPWM_DEFINE_CHANNEL(14, DDRC, PORTC, PORTC0);  //Arduino pin A0
SOFTPWM_DEFINE_CHANNEL(15, DDRC, PORTC, PORTC1);  //Arduino pin A1
SOFTPWM_DEFINE_CHANNEL(16, DDRC, PORTC, PORTC2);  //Arduino pin A2
SOFTPWM_DEFINE_CHANNEL(17, DDRC, PORTC, PORTC3);  //Arduino pin A3
SOFTPWM_DEFINE_CHANNEL(18, DDRC, PORTC, PORTC4);  //Arduino pin A4
SOFTPWM_DEFINE_CHANNEL(19, DDRC, PORTC, PORTC5);  //Arduino pin A5

SOFTPWM_DEFINE_OBJECT(20);

const int pins[] = {9, 8, 7, 6};

const int pintable[][8] = {
    {1, 1, 0, 0, 0, 0, 0, 1},
    {0, 1, 1, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 1, 1, 1},
};

int step;

#if ANIMATE_LED
const int delay_min = 120;
const int delay_max = 180;
#else
const int delay_min = 0;
const int delay_max = 50;
#endif
const int delay_change = 1000;
int delay_cycle;
int delay_add;
//const int delay_add = 35;

#if ANIMATE_LED
const short led_min_val = 30;
const short led_max_val = 255;
const short led_min_time = 500;
const short led_max_time = 1000;

struct led_s {
    short start_val;
    short end_val;
    short delta_val;
    short current_time;
    short total_time;
};

struct led_s ledstate[NUMLEDS] = {0};
#endif

void setup() {
    for(int pin = 0; pin < 4; pin++) {
        pinMode(pins[pin], OUTPUT);
        digitalWrite(pins[pin], LOW);
    }

    pinMode(13, OUTPUT);
    step = 0;
    delay_cycle = 0;
    randomSeed(analogRead(7));
    delay_add = random(delay_min, delay_max);

    Palatis::SoftPWM.begin(60);

    for(int led = 0; led < NUMLEDS; ++led) {
#if ANIMATE_LED
        ledstate[led].start_val = 0;
        ledstate[led].end_val = random(led_min_val, led_max_val);
        ledstate[led].delta_val = ledstate[led].end_val - ledstate[led].start_val;
        ledstate[led].current_time = 0;
        ledstate[led].total_time = random(led_min_time, led_max_time);
#else
        Palatis::SoftPWM.set(led + 14, 255);
#endif
    }
}

void loop() {
    for(int pin = 0; pin < 4; ++pin) {
        int val = (pintable[pin][step] == 0 ? LOW : HIGH);
        digitalWrite(pins[pin], val);
    }
    step = (step + 1) % 8;

    if (delay_cycle >= delay_change) {
        delay_add = random(delay_min, delay_max);
        delay_cycle = 0;
    }
    delay_cycle ++;

    for(int i = 0; i < delay_add; ++i) {
        digitalWrite(13, LOW);
    }

#if ANIMATE_LED
    for(int led = 0; led < NUMLEDS; ++led) {
        int ledval = (int) ((float) ((float) ledstate[led].current_time / (float) ledstate[led].total_time) * ledstate[led].delta_val) + ledstate[led].start_val;
        Palatis::SoftPWM.set(led + 14, ledval);
        if(ledstate[led].current_time < ledstate[led].total_time) {
            ledstate[led].current_time ++;
        } else {
            ledstate[led].start_val = ledstate[led].end_val;
            ledstate[led].end_val = random(led_min_val, led_max_val);
            ledstate[led].delta_val = ledstate[led].end_val - ledstate[led].start_val;
            ledstate[led].current_time = 0;
            ledstate[led].total_time = random(led_min_time, led_max_time);
        }
    }
#else
    delay(1);
#endif
}
